/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"bufio"
	"github.com/spf13/cobra"
	"golang.org/x/oauth2"
	"os"
)

// deleteApiruleCmd represents the deleteApirule command
var deleteApiruleCmd = &cobra.Command{
	Use:   "delete-apirule",
	Short: "Delete an api stream rule",
	Long: `delete api-rule <API number>

	delete api-rule 5
	`,
	Run: func(cmd *cobra.Command, args []string) {
		defer func() {
			if r := recover(); r != nil {
				fmt.Printf("Enter in a rule number \n")
			}
		}()
		delRule(args[0])
	},
}

func init() {
	rootCmd.AddCommand(deleteApiruleCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// deleteApiruleCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// deleteApiruleCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func delRule(input string) {

	getRules()

	lenRules := len(RuleResp.Rules)

	if lenRules <= 0 {
		fmt.Println("There are no rules")
	} else {
		fmt.Println("Total API Rules:", lenRules)

		numInput, err := strconv.Atoi(input)
		
		if err != nil {
			fmt.Println(input, "is not a valid API Rule number")
			fmt.Println("Valid API Rule numbers are:")
		}

		if numInput > lenRules {
			fmt.Println(input, "is not a valid API Rule number")
			fmt.Println("Valid API Rule numbers are:")
		}

		ruleID := RuleResp.Rules[numInput].ID

		fmt.Println("RULE ID ========>", ruleID)

		fmt.Println("Are you sure you want to delete rule: \n")
		fmt.Println("ID:", RuleResp.Rules[numInput].ID)
		fmt.Println("Value: ", RuleResp.Rules[numInput].Value) 
		fmt.Println("Tag:", RuleResp.Rules[numInput].Tag) 


		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		text := scanner.Text()
		
		if text == "yes" {
			ctx := context.Background()

			Client := oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{
				AccessToken: Cfg.Config.BearerToken,
				TokenType:   "Bearer",
			}))
		
			payload := Payload{
				delete{
					Ids: []string{ruleID},
				},
			}
		
			p1, _ := json.Marshal(payload)
		
			//Post method for http Client type
			//func (c *Client) Post(url, contentType string, body io.Reader) (resp *Response, err error)
			_, err := Client.Post(rulesURL, "application/json", bytes.NewBuffer([]byte(p1)))
			if err != nil {
				fmt.Println(err)
			}
		} else {
			fmt.Println("No rules were deleted.")
			os.Exit(1)
		}
	}
}



