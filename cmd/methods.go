package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
)

func (a *configFile) readConf() *configFile {

	yamlFile, err := ioutil.ReadFile(configFileDir() + "/" + ".tweetctl" + "/" + YamlConf)
	if err != nil {
		fmt.Println(err)
	}

	err = yaml.Unmarshal(yamlFile, a)
	if err != nil {
		fmt.Println(err)
	}

	return a
}

func configFileDir() string {
	d, err := os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
	}
	return d
}
