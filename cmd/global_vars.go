//Contains global variables used in tweetctl

package cmd

import (
	"net/http"
	texttemplate "text/template"
)

var CfgFile string

var ConfTpl *texttemplate.Template

//YamlConf sets the global config file var name
var YamlConf = "config.yaml"

//See structs.go
var RuleResp ruleType

//See structs.go
var Cfg configFile

var Client *http.Client
