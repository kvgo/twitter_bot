package cmd

type configFile struct {
	Config struct {
		APIKey      string `yaml:"api_key"`
		APISecret   string `yaml:"api_secret"`
		BearerToken string `yaml:"api_bearertoken"`
	} `yaml:"config"`
}

type ruleType struct {
	Rules []struct {
		ID    string `json:"id"`
		Value string `json:"value"`
		Tag   string `json:"tag"`
	} `json:"data"`
}

type delete struct {
	Ids []string `json:"ids"`
}

type Payload struct {
	Delete delete `json:"delete"`
}

type AddPayload struct {
	NewRule []NewRule `json:"add"`
}

type NewRule struct {
	Value string `json:"value"`
	Tag string `json:"tag"`
}
