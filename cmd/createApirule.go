/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"golang.org/x/oauth2"
	"gopkg.in/yaml.v3"
)

// createApiruleCmd represents the createApirule command
var createApiruleCmd = &cobra.Command{
	Use:   "create-apirule",
	Short: "Creates API Rule",
	Long: `Create an API Rule 

		tweetctl create-apirule --value --tag
	.`,
	Run: func(cmd *cobra.Command, args []string) {
		defer func() {
			if r := recover(); r != nil {
				fmt.Printf("Enter in a value \n")
			}
		}()
		createRule()
	},
}

func init() {
	rootCmd.AddCommand(createApiruleCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createApiruleCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createApiruleCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func createRule() {

	ctx := context.Background()

	Client := oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{
		AccessToken: getBtoken(),
		TokenType:   "Bearer",
	}))

	payload := AddPayload{
		NewRule: []NewRule{
			{Value: "cat has:media",
				Tag: "cats with media"},
		},
	}

	p1, _ := json.Marshal(payload)

	_, err := Client.Post(rulesURL, "application/json", bytes.NewBuffer([]byte(p1)))
	if err != nil {
		fmt.Println(err)
	}
}

func getBtoken() string {

	configFileDir, err := os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
	}

	//Btoken is a *os.File
	Btoken, err := os.Open(configFileDir + "/" + ".tweetctl" + "/" + YamlConf)
	if err != nil {
		fmt.Println(err)
	}
	defer Btoken.Close()

	//Decode / unmarshall the YAML file
	decoder := yaml.NewDecoder(Btoken)
	err = decoder.Decode(&Cfg)
	if err != nil {
		fmt.Println(err)
	}

	return Cfg.Config.BearerToken
}
