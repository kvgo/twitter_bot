/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	texttemplate "text/template"
)

// setBearertokenCmd represents the setBearertoken command
var setBearertokenCmd = &cobra.Command{
	Use:   "set-bearertoken",
	Short: "Sets the bearer token to access the API",
	Long: `Sets the bearer token to access the API
	        Example:
			tweetctl set-bearertoken <bearer_token>`,
	Run: func(cmd *cobra.Command, args []string) {
		//Using a defer to control the panic gracefully
		defer func() {
			if r := recover(); r != nil {
				fmt.Printf("Enter BearerToken")
			}
		}()
		setBearertoken(args[0])
	},
}

func init() {
	rootCmd.AddCommand(setBearertokenCmd)

	ConfTpl = texttemplate.Must(texttemplate.ParseFiles("conf.tmpl"))

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setBearertokenCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// setBearertokenCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func setBearertoken(input string) {

	Cfg.readConf()

	confFile, err := os.OpenFile(configFileDir()+"/"+".tweetctl"+"/"+YamlConf, os.O_RDWR, 0666)
	if err != nil {
		fmt.Println(err)
	}
	defer confFile.Close()

	f, err := os.OpenFile(configFileDir()+"/"+".tweetctl"+"/"+YamlConf, os.O_RDWR, 0666)
	if err != nil {
		fmt.Println(err)
	}

	apiAuth := map[string]string{
		"apiKey":      Cfg.Config.APIKey,
		"apiSecret":   Cfg.Config.APISecret,
		"bearerToken": input,
	}

	err = ConfTpl.ExecuteTemplate(f, "conf.tmpl", apiAuth)
	if err != nil {
		fmt.Println("setapikey", err)
	}

}
