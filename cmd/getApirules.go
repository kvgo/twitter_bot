/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"

	"github.com/spf13/cobra"
	"golang.org/x/oauth2"
	"gopkg.in/yaml.v3"
)

const (
	streamURL string = "https://api.twitter.com/2/tweets/search/stream"
	rulesURL  string = "https://api.twitter.com/2/tweets/search/stream/rules"
)

// getApirulesCmd represents the getApirules command
var getApirulesCmd = &cobra.Command{
	Use:   "get-apirules",
	Short: "Retrieves API Rules set for a stream",
	Long: `Retrieves the API Rules set for a stream. It uses the following endpoint: /2/tweets/search/stream/rules
		   Example:
		   tweetctl get-apirules`,
	Run: func(cmd *cobra.Command, args []string) {
		getRules()
	},
}

func init() {
	rootCmd.AddCommand(getApirulesCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getApirulesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getApirulesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func getRules() {
	ctx := context.Background()

	configFileDir, err := os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
	}

	//Btoken is a *os.File
	Btoken, err := os.Open(configFileDir + "/" + ".tweetctl" + "/" + YamlConf)
	if err != nil {
		fmt.Println(err)
	}
	defer Btoken.Close()

	//Decode / unmarshall the YAML file
	decoder := yaml.NewDecoder(Btoken)
	err = decoder.Decode(&Cfg)
	if err != nil {
		fmt.Println(err)
	}

	Client := oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{
		//Cfg is the var
		//Config is a field in the struct
		//BearerToken is a field in the sub struct
		AccessToken: Cfg.Config.BearerToken,
		TokenType:   "Bearer",
	}))

	//Retrieve the rules using the rules endpoint
	rawData, err := Client.Get(rulesURL)
	if err != nil {
		fmt.Println(err)
	}

	//Read the body from the rawData
	ruleData, err := ioutil.ReadAll(rawData.Body)
	rawData.Body.Close()
	if err != nil {
		fmt.Println(err)
	}

	if rawData.Status != "200 OK" {
		fmt.Println(rawData.Status)
	} else {

		//Unmarshal the json into the rule struct
		//see structs.go
		err = json.Unmarshal(ruleData, &RuleResp)
		if err != nil {
			fmt.Println(err)
		}

		//Checks to see what the caller function is
		pc, _, _, ok := runtime.Caller(1)
		details := runtime.FuncForPC(pc)
		if ok && details != nil {

	//If the delRule() function/command called the getRule function do nothing
	//Otherwise print the rules
	if details.Name() == "gitlab.com/kvgo/tweetctl/cmd.delRule" {

			} else {
				printRules()
			}
		}
	}
}

func printRules() {
	//fmt.Println("fmt.Println(ID: , rules.Rule[0].ID)", "ID: ", rules.Rule[0].ID)
	//fmt.Println("Value: ", rule.Rule[0].Value)
	//fmt.Println("Tag: ", rule.Rule[0].Tag)

	if len(RuleResp.Rules) == 0 {
		fmt.Println("There are no rules ")
	} else {

		fmt.Println("=============================")
		fmt.Println("===== TWITTER API RULES =====")
		fmt.Println("=============================")
	}
	
	for key := range RuleResp.Rules {
		fmt.Println(key, "  ID:", RuleResp.Rules[key].ID)     //rules.Rule[slice index position].ID
		fmt.Println("    Value: ", RuleResp.Rules[key].Value) //rules.Rule[slice index position].Value
		fmt.Println("    Tag: ", RuleResp.Rules[key].Tag)     //rules.Rule[slice index position].Tag
		fmt.Println("======================================================")
		}
}
