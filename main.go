/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/kvgo/tweetctl/cmd"
)

//Used for testing
//var apiKey = "zxmuWzdj4Lg7tu6uatW1ob6cC"
//var apiSecret = "pJmcrgj2gXPWXkAhQephYeEeESNfHeJc2xkTW0EY2uKbEJHPKn"
//var bearerToken = "AAAAAAAAAAAAAAAAAAAAAAp6MwEAAAAAE7Ubj1lmCA8jPArw5tsS7SWpVZY%3DXq4f6Qxvez17kGI5CHFU0EA7t263vaq95iYXjL6a5bcC4fmnSh"

func init() {

}

func main() {

	cmd.Execute()
	createConf()

}

func createConf() {
	homeDirectory, err := os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
	}

	confDir := homeDirectory + "/" + ".tweetctl"

	homeDirDirs, err := ioutil.ReadDir(homeDirectory)
	if err != nil {
		fmt.Println(err)
	}

	//check if the config dir is in the users home directory
	if contains(homeDirDirs, ".tweetctl") {
		confDirFiles, err := ioutil.ReadDir(confDir)
		if err != nil {
			fmt.Println(err)
		}
		//If the config file exists do nothing
		if contains(confDirFiles, cmd.YamlConf) {

		} else {
			os.Chdir(confDir)
			config, err := os.Create(cmd.YamlConf)
			if err != nil {
				fmt.Println(err)
			}
			err = cmd.ConfTpl.ExecuteTemplate(config, "conf.tmpl", "")
			if err != nil {
				log.Fatalln(err)
			}
		}
	} else {
		os.Chdir(homeDirectory)
		os.Mkdir(confDir, 0700)

		//config, err := os.Create(confDir + "/" + confFile)
		config, err := os.OpenFile(confDir+"/"+cmd.YamlConf, os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
		}

		apiAuth := map[string]string{
			"apiKey":      "",
			"apiSecret":   "",
			"bearerToken": "",
		}

		err = cmd.ConfTpl.ExecuteTemplate(config, "conf.tmpl", apiAuth)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func contains(s []os.FileInfo, confFile string) bool {
	for _, a := range s {
		if a.Name() == confFile {
			return true
		}
	}
	return false
}
