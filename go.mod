module gitlab.com/kvgo/tweetctl

go 1.15

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	golang.org/x/oauth2 v0.0.0-20210220000619-9bb904979d93
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
